<?php

use Phalcon\Http\Response;
use Phalcon\Mvc\Controller;

class RoleController extends Controller
{
    public function initialize()
    {
        $this->view->setTemplateAfter('navbar');
    }

    public function indexAction()
    {
        $this->view->title = 'List Roles';

        $this->view->roles = Role::find();
    }

    public function createAction()
    {
        $this->view->title = 'Create Role';

    }

    public function storeAction()
    {
        $role = new Role();
        $role->name = $this->request->getPost('name');
        $role->save();
        
        return $this->response->redirect('role');
    }

    public function editAction($id)
    {
    	$role = Role::findFirstById($id);

        $this->view->title = "Edit Role: $role->name";

    	$this->view->role = $role;
    }

    public function updateAction($id)
    {
    	$role = Role::findFirstById($id);

    	if(!$role) {
    		$this->flash->error("Role not found");

            return $this->response->redirect('role');
    	}

    	$role->name = $this->request->getPost('name');
    	$role->save();

        return $this->response->redirect('role');
    }

    public function deleteAction($id)
    {
    	$role = Role::findFirstById($id);

    	if(!$role) {
    		$this->flash->error("Role not found");

            return $this->response->redirect('role');
    	}

    	$role->delete();

    	return $this->response->redirect('role');
    }
}