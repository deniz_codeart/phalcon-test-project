<?php

use Phalcon\Mvc\Controller;

class IndexController extends Controller
{
    public function initialize()
    {
        $this->view->setTemplateBefore('navbar');
    }

    public function indexAction()
    {
        $this->view->users = User::find();
    }
}