<?php

use Phalcon\Mvc\Controller;

class RegisterController extends Controller
{    
    public function initialize()
    {
        $this->view->setTemplateAfter('navbar');
    }

    public function indexAction()
    {
        $this->view->title = 'Register';

        $this->view->pick("auth/register");
    }

    public function storeAction()
    {
        $user = new User();

        $role = Role::findFirst([ "name = 'patient'" ]);
        
        $user->role_id = $role->id;
        $user->name = $this->request->getPost('name');
        
        $user->email = $this->request->getPost('email');
        $userExists = User::count([ "email = '$user->email'" ]) == 0 ? false : true;
        if($userExists) {
            $this->view->message = "User with the email $user->email already exists.";
            $this->view->success = $success;
            $this->response->redirect('register');
        }

        $user->password = $this->security->hash($this->request->getPost('password'));

        // Store and check for errors
        $success = $user->save();

        // passing the result to the view
        $this->view->success = $success;

        if (!$success) {
            $message = "Sorry, the following problems were generated:<br>"
                     . implode('<br>', $user->getMessages());
            $this->view->message = $message;
        } else {
            $this->response->redirect('user');
        }
    }
}