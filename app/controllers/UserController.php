<?php

use Phalcon\Http\Response;
use Phalcon\Mvc\Controller;

class UserController extends Controller
{
    public function initialize()
    {
        $this->view->setTemplateAfter('navbar');
    }
    
    public function indexAction()
    {
        $this->view->title = 'List Users';

        $this->view->users = User::find();
    }

    public function createAction()
    {
        $this->view->title = 'Create User';

        $this->view->roles = Role::find();
    }

    public function storeAction()
    {
        $user = new User();

        $user->role_id = $this->request->getPost('role_id');
        $user->name = $this->request->getPost('name');
        $user->email = $this->request->getPost('email');
        $user->password = $this->security->hash($this->request->getPost('password'));

        $user->save();
        
        return $this->response->redirect('user');
    }

    public function editAction($id)
    {

    	$user = User::findFirstById($id);

        $this->view->title = "Edit User: $user->name";

    	$this->view->user = $user;
        $this->view->roles = Role::find();
    }

    public function updateAction($id)
    {
    	$user = User::findFirstById($id);

    	if(!$user) {
    		$this->flash->error("User not found");

            return $this->response->redirect('user');
    	}

    	$user->name = $this->request->getPost('name');
    	$user->email = $this->request->getPost('email');
    	$user->save();

        
        return $this->response->redirect('user');
    }

    public function deleteAction($id)
    {
    	$user = User::findFirstById($id);

    	if(!$user) {
    		$this->flash->error("User not found");

            return $this->response->redirect('user');
    	}

    	$user->delete();

    	return $this->response->redirect('user');
    }
}