<?php

use Phalcon\Mvc\Controller;

class SignupController extends Controller
{
    public function initialize()
    {
        $this->view->setTemplateBefore('navbar');
    }
    
    public function registerAction()
    {

    }



    public function storeAction()
    {
        $user = new User();

        //assign value from the form to $user
        // $user->assign(
        //     $this->request->getPost(),
        //     [
        //         'role_id',
        //         'name',
        //         'email'
        //     ]
        // );
        $user->role_id = $this->request->getPost('role_id');
        $user->name = $this->request->getPost('name');
        $user->email = $this->request->getPost('email');

        // Store and check for errors
        $success = $user->save();

        // passing the result to the view
        $this->view->success = $success;

        if ($success) {
            $message = "Thanks for registering!";
        } else {
            $message = "Sorry, the following problems were generated:<br>"
                     . implode('<br>', $user->getMessages());
        }

        // passing a message to the view
        $this->view->message = $message;
    }
}