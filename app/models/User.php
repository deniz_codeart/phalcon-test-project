<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\Timestampable;

class User extends Model
{
    public $id;
    public $role_id;
    public $name;
    public $email;
    public $password;

    public function initialize()
    {
        $this->setSource('users');

        $this->belongsTo(
            "role_id",
            Role::class,
            "id",
            [
            	"alias"    => "role"
            ]
        );

        $this->addBehavior(
            new Timestampable(
                [
                    'beforeCreate' => [
                        'field'  => 'created_at',
                        'format' => 'Y-m-d H:i:s',
                    ],
                    'beforeCreate' => [
                        'field'  => 'updated_at',
                        'format' => 'Y-m-d H:i:s',
                    ]
                ]
            )
        );
    }
}