<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\Timestampable;

class Role extends Model
{
    public $id;
    public $name;

    public function initialize()
    {
    	$this->setSource('roles');

        $this->hasMany(
            "id",
            User::class,
            "role_id",
            [
            	"alias"    => "users"
            ]
        );

        $this->addBehavior(
            new Timestampable(
                [
                    'onCreate' => [
                        'field'  => 'created_at',
                        'format' => 'Y-m-d H:i:s',
                    ],
                    'beforeCreate' => [
                        'field'  => 'updated_at',
                        'format' => 'Y-m-d H:i:s',
                    ]
                ]
            )
        );
    }
}